import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

@SuppressWarnings("SpellCheckingInspection")
public class TestSelenium {

    private static final String SEARCH_TEXT = "automation";

    public static void main(String... args) throws InterruptedException {
        /* Я использую linux, windows не проверял но поидее должно работать и на windows */
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("linux")) {
            System.setProperty("webdriver.chrome.driver", "driver/chromedriver");
        } else if (os.contains("windows")) {
            System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
        } else {
            System.err.println("Неподдерживаемая операционная система!");
        }

        WebDriver driver = new ChromeDriver();
        driver.get("https://www.bing.com/");

        WebElement elementSearchBox = driver.findElement(By.className("b_searchbox"));
        elementSearchBox.sendKeys(SEARCH_TEXT);

        WebElement elementButtonSearch = driver.findElement(By.className("b_searchboxSubmit"));
        elementButtonSearch.submit();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("b_results")));

        System.out.println("Заголовок: " + driver.getTitle());

        List<WebElement> b_algo = driver.findElements(By.className("b_algo"));

        if (!b_algo.isEmpty()) {
            System.out.println("Результат:\n");
            for (WebElement element : b_algo) {
                System.out.println(element.findElement(By.tagName("h2")).getText());
            }
        } else System.err.println("List пустой! Ничего не найдено!");
        driver.quit();
    }
}
